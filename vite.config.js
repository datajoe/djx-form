import path from 'path';
import vue from '@vitejs/plugin-vue';

export default {
  plugins: [vue()],
  build: {
    lib: {
      entry: path.resolve(__dirname, 'src/index.js'),
      name: 'djx-form',
      fileName: (format) => `djx-form.${format}.js`
    },
    rollupOptions: {
      input: 'src/index.js',
      external: [
        'vue',
        'vee-validate',
        'vuex',
      ],
      output:{
        globals: {
          vue: 'Vue',
          vuex: 'Vuex',
          'vee-validate': 'veeValidate',
        },
      },
    },
  },
};
