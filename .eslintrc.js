module.exports = {
  root: true,
  env: {
    browser: true,
    node: true,
    es6: true,
  },
  extends: [
    'plugin:vue/vue3-essential',
  ],
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-underscore-dangle': 'off',
    'no-param-reassign': ['error', { ignorePropertyModificationsFor: ['state'] }],
    camelcase: ['off'],
    'import/no-extraneous-dependencies': ['off'],
    'vue/multi-word-component-names': ['off'],
    'vue/html-indent': ['error', 2],
  },
  parserOptions: {
    parser: '@babel/eslint-parser',
    sourceType: 'module',
    requireConfigFile: false,
  },
};
