import { mapState } from 'vuex';

export default {
  methods: {
    prepopulate() {
      if (
        this.field.prepopulated
        && this.secret
        && this.prepopulation
        && this.prepopulation[this.secret]
        && this.prepopulation[this.secret][this.field.name]
      ) {
        this.value = this.prepopulation[this.secret][this.field.name];
      }
    },
  },
  computed: {
    ...mapState([
      'prepopulation',
    ]),
    secret() {
      return this.$route.params.secret;
    },
  },
};
