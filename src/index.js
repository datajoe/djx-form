import './init/validation';

export { default as FormInput } from './components/Form/Input.vue';
export { default as FormTextArea } from './components/Form/TextArea.vue';
export { default as FormCheckbox } from './components/Form/Checkbox.vue';
export { default as FormHtml } from './components/Form/Html.vue';
export { default as FormRadio } from './components/Form/Radio.vue';
export { default as FormFancyRadio } from './components/Form/FancyRadio.vue';
export { default as FormSelect } from './components/Form/Select.vue';
export { default as FormSlider } from './components/Form/Slider.vue';
export { default as FormFile } from './components/Form/File.vue';
