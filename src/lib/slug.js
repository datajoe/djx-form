import slugify from '@sindresorhus/slugify';

export default string => slugify(string, { decamelize: false });
